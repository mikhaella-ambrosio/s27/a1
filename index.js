console.log(`s27 Activity - Node.js Routing with HTTP Methods`);

const HTTP = require(`http`);
const port = 3000;
let courses = [`Math`, `Science`];
HTTP.createServer((request, response) => {
    if (request.url === "/" && request.method == `GET`){
        response.writeHead(200, {"Content-Type": `text/plain`});
        response.write(`Welcome to Booking System`);
        response.end();
    } else if (request.url == `/profile` && request.method == `GET`) {
        response.writeHead(200, {"Content-Type": `text/plain`});
        response.write(`Welcome to your profile!`);
        response.end();
    } else if (request.url == `/courses` && request.method == `GET`) {
        response.writeHead(200, {"Content-Type": `text/plain`});
        response.write(`Here are the courses available:`);
        response.end();
    } else if (request.url == `/addcourse` && request.method == `POST`) {
        let reqBody = "";

        request.on(`data`, (data) => {
            reqBody += data
        }) 

        request.on(`end`, () => {
            
            reqBody = JSON.parse(reqBody)
            courses.push(reqBody)
    
            const sentence = `Add course to our resources: `

            // console.log(courses)
    
            response.writeHead(200, {"Content-Type": `text/plain`})
            response.write(sentence)
            response.end()
        })
    } else if (request.url == `/updatecourse` && request.method == `PUT`) {
        let reqBody = "";

        request.on(`data`, (data) => {
            reqBody += data
        }) 
        request.on(`end`, () => {
            
            reqBody = JSON.parse(reqBody)
            // courses.set(reqBody)
    
            const sentence = `Update a course to our resources:`

            // console.log(courses)
    
            response.writeHead(200, {"Content-Type": `text/plain`})
            response.write(sentence)
            response.end()
        })
       
    } else if (request.url == `/archivecourse` && request.method == `DELETE`) {
        let reqBody = "";

        request.on(`data`, (data) => {
            reqBody += data
        }) 
        request.on(`end`, () => {
            
            reqBody = JSON.parse(reqBody)
            // courses.set(reqBody)
    
            const sentence = `Archive courses to our resources `

            console.log(courses)
    
            response.writeHead(200, {"Content-Type": `text/plain`})
            response.write(sentence)
            response.end()
        })
       
    }
    
}).listen(port, () => console.log(`Server connected to port ${port}`));